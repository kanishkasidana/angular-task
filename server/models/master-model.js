const Sequelize = require('sequelize');
const sequelize = require('../config/db');

const Master = sequelize.define('master', {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
    },
    name: {
        type: Sequelize.TEXT
    },
    type: {
        type: Sequelize.TEXT
    }
}, {
        freezeTableName: true,
        tableName: 'master'
    });

module.exports = {
    Master: Master
};