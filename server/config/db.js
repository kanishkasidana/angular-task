const Sequelize = require('sequelize');

var sequelize = new Sequelize(process.env.REACT_APP_DB_NAME, process.env.REACT_APP_DB_USER, process.env.REACT_APP_DB_PASSWORD, {
    host: process.env.REACT_APP_DB_HOST,
    dialect: 'mysql',
    operatorsAliases: false,
    //insecureAuth:true,  //allow connectikon to http  
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});


module.exports = sequelize;