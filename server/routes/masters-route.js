const express = require('express');
const router = express.Router();

var masterController = require('../controllers/master-controller');

//get master
router.get('/:type', masterController.getMasterByType);

//add to master
router.post('/add', masterController.addMasterData);

// router.get('/', masterController.getMaster);

module.exports = router;