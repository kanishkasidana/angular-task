const express = require('express');
const router = express.Router();
const multer = require('multer');

var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, __basedir + '/uploads/')
    },
    filename: (req, file, cb) => {
        // console.log("file", file);
        cb(null, file.originalname)
    }
});

var upload = multer({ storage: storage });

var usersController = require('../controllers/user-controller');

//add user
router.post('/save', usersController.saveUser);

//get users
router.get('/', usersController.getUsers);

//delete user by id
router.delete('/:userId', usersController.deleteUser);

//get user by id
router.post('/get', usersController.getUserById);

//upload profile photo
router.post('/upload', upload.single("image"), usersController.uploadProfile);

module.exports = router;