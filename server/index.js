require('dotenv').config();

const express = require('express');
const sequelize = require('./config/db');
const bodyParser = require('body-parser');

const app = express();

const cors = require('cors');
const corsOptions = {
    origin: 'http://localhost:4200',
    optionsSuccessStatus: 200
}

app.use(cors(corsOptions));

// global.__basedir = path.resolve("../src/assets");
global.__basedir = __dirname;
// console.log("__basedir", __basedir);
// console.log(". = %S", path.resolve("../src/assets/uploads")); //change the path of dirname**

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

// app.use(express.static('dist'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.use('/users', require('./routes/users-route'));
app.use('/master', require('./routes/masters-route'));
// app.use('/api/ruleSet', require('./routes/rule-set/rule-set-route'));


app.listen(3001, () => {
    sequelize.sync()

    console.log("Server listening at port 3001");
})