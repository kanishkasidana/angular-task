const { User } = require('../models/users-model');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const generateUuid = require('../utils/uuid');

exports.saveUser = (req, res) => {
    // console.log("body", req.body.id);
    User.findOne({
        where: { id: req.body.id }
    })
        .then((data) => {
            // console.log("data", data);
            if (data) {
                User.update(req.body,
                    { where: { id: req.body.id } })
                    .then(() => {
                        res.json({ "msg": "User updated successfully" })
                    }).catch((err) => {
                        res.json({ "err": "Something went wrong. Please try again later" });
                    })
            } else {
                req.body.id = generateUuid.uuidv4();
                // console.log("req.body", req.body);
                const userDetails = User.build(req.body);
                userDetails.save()
                    .then(() => {
                        res.json({ "msg": "User detail saved successfully" })
                    }).catch((err) => {
                        res.json({ "err": "Something went wrong. Please try again later" });
                    })
            }
        })
        .catch((err) => {
            // console.log("err", err);
            res.json({ "err": "Something went wrong. Please try again later" });
        })

}

exports.getUsers = (req, res) => {
    // console.log("in  here");
    User.findAll({
        attributes: ["profile", "name", "surname", "email", "skills", "id"]
    })
        .then((result) => {
            // console.log("result", result);
            if (result) {
                res.json(result);
            }
        })
        .catch((err) => {
            // console.log("error");
            res.json({ "err": "Something went wrong. Please try again later" });
        })
}

exports.deleteUser = (req, res) => {
    // console.log("req.params", req.params.userId);

    User.destroy({
        where: { id: req.params.userId }
    }).then(() => {
        res.json({ msg: "user deleted successfully" });
    }).catch((err) => {
        // console.log("err", err);
        res.json({ "err": "Something went wrong. Please try again later" });
    })
}

exports.getUserById = (req, res) => {
    // console.log("body", req.body);
    User.findOne({
        where: { email: req.body.email }
    })
        .then((result) => {
            // console.log("result", result.dataValues);
            if (result) {
                res.json(result);
            }
        })
        .catch((err) => {
            // console.log("err", err);
            res.json({ "err": "Something went wrong. Please try again later" });
        })
}

exports.uploadProfile = (req, res) => {
    // console.log("req.file", req.file);
    res.json({ msg: "File Uploaded successfully" });
}
