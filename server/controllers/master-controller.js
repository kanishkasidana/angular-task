const { Master } = require('../models/master-model');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

exports.getMasterByType = (req, res) => {
    // console.log("in master", req.params.type);

    Master.findAll({
        attributes: ['id', 'name'],
        where: { type: req.params.type }
    })
        .then((result) => {
            // console.log("result", result);
            if (result) {
                res.json(result);
            }
        })
        .catch((err) => {
            // console.log("err", err);
            res.json({ err: "Something went wrong, please try again later" });
        })

}

// exports.getMaster = (req, res) => {
//     Master.findAll({
//         attributes: ['id', 'name', 'type']
//     })
//         .then((result) => {
//             console.log("result", result);
//             res.json(result);
//         })
//         .catch((err) => {
//             console.log("err", err);
//         })
// }

exports.addMasterData = (req, res) => {
    // console.log("req.body", req.body);

    const masterData = Master.build(req.body);
    masterData.save()
        .then(() => {
            res.json({ "msg": `Added ${req.body.type} successfully` })
        }).catch((err) => {
            res.json({ "err": "Something went wrong. Please try again later" });
        })
}