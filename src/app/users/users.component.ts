import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';

declare var require: any
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: User[] = [];
  header = [];
  rows = [];
  imagePath = "assets/default_user.jpg";

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    return this.userService.getUsers().subscribe(users => {
      if (users !== undefined && users !== null && users.length > 0) {
        console.log("users", users);
        let imageUsers = users.map((u) => {
          if (u.profile) {
            // u.imageSrc = `assets/uploads/${u.profile}`; // image access from assets folder directly
            u.imageSrc = require('../../../server/uploads/' + u.profile);
          }
          return u;
        })

        this.users = users;

        let keys = Object.keys(users[0]);

        this.header = keys.filter((k) => {
          return k !== "id" && k !== 'profile' && k !== 'imageSrc';
        });

        // this.header = filteredKeys.map((k) => {
        //   return k.charAt(0).toUpperCase() + k.slice(1);
        // })
      }
    })
  }

  onDeleteUser(userId) {
    // console.log("userId", userId);
    if (window.confirm('Are you sure you want to delete this user?')) {
      this.userService.deleteUser(userId).subscribe(data => {
        // console.log("data", data);
        this.getUsers();
      })
    }
  }

}
