// import { Master } from './master';

export class User {
    id: string;
    name: string;
    gender: string;
    surname: string;
    city: string;
    email: string;
    skills: string;
    profile: string;
    imageSrc: any;
}