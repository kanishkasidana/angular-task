import { Master } from './master';

export const CITIES: Master[] = [
    { id: 1, name: 'Mumbai' },
    { id: 2, name: 'Delhi' },
    { id: 3, name: 'Kolkata' },
    { id: 4, name: 'Chennai' },
    { id: 5, name: 'Kerela' },
    { id: 6, name: 'Chandigarh' },
    { id: 7, name: 'Manali' },
    { id: 8, name: 'Amritsar' },
    { id: 9, name: 'Indore' }
]