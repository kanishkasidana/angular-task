import { Component, OnInit, ViewChild } from '@angular/core';
import { CITIES } from '../mock-cities';
import { Master } from '../master';
import { SKILLS } from '../mock-skills';
import { UserService } from '../user.service';
import { User } from '../user';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MasterService } from '../master.service';

declare var require: any;
@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.css']
})
export class UsersFormComponent implements OnInit {

  cities: Master[] = [];
  skills: any[] = [];

  selectedDisplayPage = "user";
  selectedSkills: any = [];

  details: User = {
    id: '',
    name: '',
    gender: '',
    surname: '',
    city: '',
    email: '',
    skills: '',
    profile: '',
    imageSrc: ''
  }

  uploadFile: File = null;
  viewPage = false;
  editPage = false;
  messageSuccess = "";
  messageErr = "";
  showDialog = false;
  dialogType = "";
  dialogInput = "";
  dialogMessageSuccess = "";
  dialogMessageErr = "";
  fileMessageSuccess = "";
  // imageSrc: any;

  constructor(private userService: UserService,
    private route: ActivatedRoute,
    private location: Location,
    private masterService: MasterService) { }

  ngOnInit() {
    // this.getMasterData();
    this.getMasterDataByType('city');
    this.getMasterDataByType('skill');
    const email = this.route.snapshot.paramMap.get('email');
    // console.log("this.route.snapshot", this.route.snapshot);
    let flagEdit = this.route.snapshot.data.flag;
    // console.log("email", email);
    if (email) {
      this.getUser(email);
      if (!flagEdit) {
        this.viewPage = true;
      } else {
        this.editPage = true;
      }
    }
  }

  // getMasterData() {
  //   this.masterService.getMaster().subscribe((data: any) => {
  //     console.log("data", data);
  //     if (data !== null && data !== undefined && data.length > 0) {
  //       for (let i = 0; i < data.length; i++) {
  //         if (data[i].type === 'city') {
  //           let dataObj = {
  //             id: data.id,
  //             name: data.name
  //           }
  //           this.cities.push(dataObj);
  //         } else if (data[i].type === 'skill') {
  //           this.skills = data.map((s) => {
  //             let sObj = {
  //               id: s.id,
  //               name: s.name,
  //               checked: false
  //             }
  //             // s["checked"] = false;
  //             return sObj;
  //           });
  //           console.log("skills from master with checked", this.skills);
  //         }
  //       }
  //     }
  //   })
  // }

  getMasterDataByType(type) {
    this.masterService.getMasterByType(type).subscribe((data: any) => {
      if (data !== null && data !== undefined) {
        // console.log("citiesfrom master", data);
        if (type === 'city') {
          this.cities = data;
        } else if (type === 'skill') {
          this.skills = data.map((s) => {
            s["checked"] = false;
            return s;
          });
          // console.log("skills from master with checked", this.skills);
        }
      }
    })
  }

  onShowCityDialog() {
    this.showDialog = !this.showDialog;
    this.dialogType = "city";
  }

  onShowSkillDialog() {
    this.showDialog = !this.showDialog;
    this.dialogType = "skill";
  }

  onSaveMaster(type) {
    // console.log("type", type);
    this.masterService.addMasterData(this.dialogInput, type).subscribe((data: any) => {
      // console.log("data", data);
      if (data && data.msg) {
        this.dialogMessageSuccess = data.msg;
      } else if (data.err) {
        this.dialogMessageErr = data.err;
      }
      this.dialogInput = "";
      this.getMasterDataByType(type);
    })
  }

  getUser(email) {
    this.userService.getUserById(email).subscribe((user: any) => {
      // console.log("user in get User", user);
      if (user !== null && user !== undefined) {
        user.imageSrc = require('../../../server/uploads/' + user.profile);
        console.log("user", user);
        this.selectedSkills = user.skills.split(",");

        this.skills = this.skills.map((s) => {
          for (let i = 0; i < this.selectedSkills.length; i++) {
            if (this.selectedSkills[i] === s.name) {
              s.checked = true;
            }
          }
          return s;
        })
        // console.log("this.skills", this.skills);
        this.details = user;
      }
    });
  }

  displayPage(page) {
    this.selectedDisplayPage = page;
  }

  onSave() {
    // console.log("this.details in parent", this.details);

    this.details.skills = this.selectedSkills.join(",");

    this.userService.saveUser(this.details).subscribe((data: any) => {
      // console.log("data", data);
      if (data !== null && data !== undefined) {
        if (data.msg) {
          this.messageSuccess = data.msg;
        } else if (data.err) {
          this.messageErr = data.err;
        }
        this.details = {
          id: '',
          name: '',
          gender: '',
          surname: '',
          city: '',
          email: '',
          skills: '',
          profile: '',
          imageSrc: ''
        }
        this.skills = this.skills.map((s) => {
          s.checked = false;
          return s;
        })
      }
    });
  }

  onCancel() {
    this.details = {
      id: '',
      name: '',
      gender: '',
      surname: '',
      city: '',
      email: '',
      skills: '',
      profile: '',
      imageSrc: ''
    }
    this.location.back();
  }

  onSkillCheck(skill) {
    // console.log("skill", skill);

    this.selectedSkills = [...this.selectedSkills, skill];
    this.skills = this.skills.map((s) => {
      if (s.name === skill) {
        s.checked = true;
      }
      return s;
    })

    // console.log("skills", this.details.skills);
  }

  handleFileInput(e) {
    // console.log("file", e.target.files);
    let selectedFile: File = e.target.files[0];
    if (selectedFile) {
      this.uploadFile = selectedFile;
      this.details.profile = this.uploadFile.name;


      const reader = new FileReader();
      reader.onload = e => this.details.imageSrc = reader.result;

      reader.readAsDataURL(selectedFile);
    }
    // console.log("uploadFile", this.uploadFile);
  }

  onFileUpload() {
    // e.prventDefault();
    let formData = new FormData();
    formData.append('image', this.uploadFile, this.uploadFile.name);
    // console.log("formData", formData);

    this.userService.uploadProfile(formData).subscribe(data => {
      // console.log("data", data);
      if (data && data.msg) {
        this.fileMessageSuccess = data.msg;
      }
    });

  }

  goBack() {
    this.location.back();
  }

}
