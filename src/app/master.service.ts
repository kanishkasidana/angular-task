import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Master } from './master';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}

@Injectable({
  providedIn: 'root'
})
export class MasterService {

  constructor(private http: HttpClient) { }
  private masterUrl = 'http://localhost:3001/master';

  // getMaster(): Observable<Master[]>{
  //   return this.http.get<Master[]>(this.masterUrl);
  // }

  getMasterByType(type: string): Observable<Master> {
    const url = `${this.masterUrl}/${type}`;
    return this.http.get<Master>(url);
  }

  addMasterData(name: string, type: string): Observable<Master> {
    const url = `${this.masterUrl}/add`;
    let data = { name: name, type: type };
    return this.http.post<Master>(url, data, httpOptions);
  }

}
