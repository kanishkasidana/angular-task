// import { Master } from './master';

export const SKILLS: any[] = [
    { id: 1, name: 'C#', checked: false },
    { id: 2, name: 'React', checked: false },
    { id: 3, name: '.Net', checked: false },
    { id: 4, name: 'Redux', checked: false },
    { id: 5, name: 'Angular', checked: false },
    { id: 6, name: 'Node', checked: false }
]