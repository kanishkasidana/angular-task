import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { UsersFormComponent } from './users-form/users-form.component';


const routes: Routes = [
  { path: '', redirectTo: 'users', pathMatch: 'full' },
  { path: 'users', component: UsersComponent },
  { path: 'users/add', component: UsersFormComponent },
  { path: 'users/:email', component: UsersFormComponent },
  { path: 'users/:email/edit', component: UsersFormComponent, data: { flag: "edit" } }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
