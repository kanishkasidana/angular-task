import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { User } from './user';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}



@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  private serverUrl = 'http://localhost:3001/users';

  // addUser(userDetails: User) {
  //   console.log("user details in service", userDetails);
  //   let url = `${this.serverUrl}/add`;
  //   console.log("url", url);

  //   return this.http.post<User>(url, userDetails, httpOptions)
  //     .pipe(tap(() => console.log("add user")),
  //     catchError(this.handleError<User>('addUser')));
  // }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any) => {
      console.log(error);
      console.log(`${operation} failed: ${error.msg}`);

      return of(result as T);
    };
  }

  // private handleError(errorResponse: HttpErrorResponse) {
  //   if (errorResponse.error.istanceOf ErrorEvent){
  //     console.error('ClientSIde eroor: ', errorResponse.error.message);
  //   } else{
  //     console.error('Server side error', errorResponse);
  //   }
  //   return new ErrorObservable("solving service error")
  // }

  saveUser(userDetail: User): Observable<User> {
    console.log("here", userDetail);
    let url = `${this.serverUrl}/save`;
    return this.http.post<User>(url, userDetail, httpOptions)
      .pipe(tap(() => console.log("add user")),
      catchError(this.handleError<any>('addUser')));
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.serverUrl);
  }

  deleteUser(userId): Observable<User> {
    const url = `${this.serverUrl}/${userId}`;

    return this.http.delete<User>(url, httpOptions);
  }

  getUserById(email): Observable<User> {
    let url = `${this.serverUrl}/get`;
    return this.http.post<User>(url, { email: email }, httpOptions);
  }

  uploadProfile(formData): Observable<any> {
    let url = `${this.serverUrl}/upload`;
    return this.http.post<any>(url, formData).pipe(tap(() => console.log("upload file")),
      catchError(this.handleError<any>('uploadFile')));
  }
}
